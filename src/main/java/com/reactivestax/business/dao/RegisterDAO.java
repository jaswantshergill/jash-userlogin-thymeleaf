package com.reactivestax.business.dao;

import com.reactivestax.business.model.UserModelObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component
public class RegisterDAO {

    @Autowired
    JdbcTemplate jdbcTemplate;

    public void register(UserModelObject user) {

        String sql = "insert into users values(?,?,?,?,?,?,?)";

        jdbcTemplate.update(sql, new Object[] { user.getUsername(), user.getPassword(), user.getFirstname(),
                user.getLastname(), user.getEmail(), user.getAddress(), user.getPhone() });

    }
}
