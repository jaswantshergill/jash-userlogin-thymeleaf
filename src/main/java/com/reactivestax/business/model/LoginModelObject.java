package com.reactivestax.business.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

public class LoginModelObject {
    @NotBlank(message = "Username can't be empty")
    @Length(min = 5, max = 10, message = "username length has to be between 5 and 10")
    private String userName;
    @NotBlank(message = "password can't be empty")
    @Length(min = 5, max = 10, message = "password length has to be between 5 and 10")
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "LoginModelObject{" +
                "userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
