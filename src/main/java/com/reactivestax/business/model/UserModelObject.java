package com.reactivestax.business.model;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

public class UserModelObject {
  @NotBlank(message = "username cannot be empty")
  private String username;
  @NotBlank(message = "password cannot be empty")
  private String password;
  @NotBlank(message = "firstname cannot be empty")
  private String firstname;
  @NotBlank(message = "lastname cannot be empty")
  private String lastname;
  @NotBlank(message = "email cannot be empty")
  private String email;
  @NotBlank(message = "address cannot be empty")
  private String address;

  private int phone;

  @Override
  public String toString() {
    return "User{" +
            "username='" + username + '\'' +
            ", password='" + password + '\'' +
            ", firstname='" + firstname + '\'' +
            ", lastname='" + lastname + '\'' +
            ", email='" + email + '\'' +
            ", address='" + address + '\'' +
            ", phone=" + phone +
            '}';
  }

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    System.out.println("username: " + username);
    this.username = username;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String firstname) {
    System.out.println("firstname: " + firstname);
    this.firstname = firstname;
  }

  public String getLastname() {
    return lastname;
  }

  public void setLastname(String lastname) {
    System.out.println("lastname: " + lastname);
    this.lastname = lastname;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public int getPhone() {
    return phone;
  }

  public void setPhone(int phone) {
    this.phone = phone;
  }
}
