package com.reactivestax.web.controllers;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class SampleController {

    private static Logger logger = LoggerFactory.getLogger(SampleController.class);

    @RequestMapping({"/sample"})
    public ModelAndView returnSample(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("sample");
        modelAndView.addObject("messagefromcontroller","real message from controller");
        System.out.println("hello");
        logger.debug("Logger runing in SampleController");
        return modelAndView;
    }
}
