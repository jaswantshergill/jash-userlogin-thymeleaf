package com.reactivestax.web.controllers;

import com.reactivestax.business.dao.RegisterDAO;
import com.reactivestax.business.model.LoginModelObject;
import com.reactivestax.business.model.UserModelObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
public class MainController {
    @Autowired
    RegisterDAO registerDAO;
    private static Logger logger = LoggerFactory.getLogger(MainController.class);

    @RequestMapping({"/"})
    public ModelAndView baseRequestMapping(){
        ModelAndView modelAndView = new ModelAndView("home");
        return modelAndView;
    }


    @RequestMapping({"/login"})
    public ModelAndView returnLogin(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login");
        modelAndView.addObject("loginModelObject", new LoginModelObject());
        logger.debug("Logger running in MainController");
        return modelAndView;
    }

    @RequestMapping({"/loginProcess"})
    public ModelAndView returnLoginProcess(@Valid final LoginModelObject loginModelObject, BindingResult bindingResult){
        ModelAndView modelAndView = new ModelAndView();

        if(bindingResult.hasErrors()){
            modelAndView.setViewName("login");
            return modelAndView;
        }

        if(loginModelObject.getUserName().equalsIgnoreCase("jaswant")){
            modelAndView.setViewName("welcome");
            modelAndView.addObject("firstName", "jaswant");
        }else{
            modelAndView.addObject("message","USername has to be jaswant");
            modelAndView.setViewName("login");
        }
        logger.debug("Logger running in MainController");
        //logger.debug(loginModelObject);
        System.out.println(loginModelObject);

        return modelAndView;
    }

    @RequestMapping({"/register"})
    public ModelAndView returnRegister(){
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("register");
        modelAndView.addObject("userModelObject", new UserModelObject());
        logger.debug("Logger running in MainController");
        return modelAndView;
    }

    @RequestMapping({"/registerProcess"})
    public ModelAndView returnRegisterProcess(@Valid final UserModelObject userModelObject, BindingResult bindingResult){
        ModelAndView modelAndView = new ModelAndView();
        if(bindingResult.hasErrors()){
            modelAndView.setViewName("register");
            return modelAndView;
        }
        logger.debug(userModelObject.toString());
        registerDAO.register(userModelObject);
        modelAndView.addObject("loginModelObject", new LoginModelObject());
        modelAndView.setViewName("login");
        logger.debug("Logger running in MainController");
        return modelAndView;
    }
}
